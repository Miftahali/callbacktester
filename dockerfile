# Menggunakan Node.js versi 14 sebagai base image
FROM node:20

# Membuat direktori kerja dalam kontainer
WORKDIR /app

# Menyalin package.json dan package-lock.json untuk instalasi dependensi
COPY package*.json ./

# Menginstal dependensi
RUN npm install

# Menyalin seluruh konten aplikasi Anda ke dalam direktori kerja
COPY . .

# Port yang akan digunakan oleh aplikasi Node.js (sesuaikan dengan aplikasi Anda)
EXPOSE 3000

# Perintah untuk menjalankan aplikasi Anda (sesuaikan dengan aplikasi Anda)
CMD [ "npm", "start" ]
