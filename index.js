const express = require('express');
const bodyParser = require('body-parser');

const app = express();

app.use(bodyParser.urlencoded({ extended: false }));

app.post('/auth/dana/callback',  (req, res, next) => {
    // Redirect atau lakukan aksi lain setelah berhasil autentikasi
    console.log(req)
    res.json({
        message: "success",
        data: req.body,
        query: req.query,
        headers: req.headers
    });
  }
);

app.get('/auth/dana/callback',  (req, res, next) => {
    // Redirect atau lakukan aksi lain setelah berhasil autentikasi
    console.log(req)
    res.json({
        message: "success",
        data: req.body,
        query: req.query,
        headers: req.headers
    });
  }
);

// Port untuk server web
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Server berjalan di http://localhost:${PORT}`);
});
